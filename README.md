# get_Slack_post

## Group TBB1

### Team:
 - Daniel Rocchi
 - Kalonte Jackson-Tate
 - Angela Phuong
 - Mary Wilson
 - Rodrigo Caldas
 
This project pulls stock information from MarketStack.com.  Once the data is received for a particular stock it is converted to a data frame and then ploted on a graph.  The graph is then used to create an image file.  The image file is then sent as a post to our Slack channel.

Our project is split into 3 files:

- data_grabber (Gets the data from MarketStack and converts to DataFrame)
- plot_image (Takes a DataFrame and create a graph)
- slack_utils (Post graph in Slack)
