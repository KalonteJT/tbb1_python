#!/usr/bin/env python
# coding: utf-8

# In[20]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pandas_datareader as pdr

get_ipython().run_line_magic('matplotlib', 'inline')


# In[21]:


def create_plots(df,stock,color1,color2,band_color):
    start = '2019'
    end = '2020'
    pd.concat([df.head(), df.tail()])
    #df['Close'].pct_change().hist(bins=50)
    # 30 days is a good approximation of a single month
    df['30d mavg'] = df['Close'].rolling(window=30).mean()
    df['30d std'] = df['Close'].rolling(window=30).std()

    df['Upper Band'] = df['30d mavg'] + (df['30d std'] * 2)
    df['Lower Band'] = df['30d mavg'] - (df['30d std'] * 2)
    # create a list of the column names we are interested in plotting

    cols = ['30d mavg','Upper Band','Lower Band', 'Close']

    df_Boll = df[cols][end]
    #df_Boll.plot()
    # set style, empty figure and axes
    plt.style.use('fivethirtyeight')
    fig = plt.figure(figsize=(12,6))
    ax = fig.add_subplot(111)

    # Get index values for the X axis for the DataFrame
    x_axis = df_Boll.index.get_level_values(0)

    # Plot shaded 'win' Day Bollinger Band for Facebook
    ax.fill_between(x_axis, 
                    df_Boll['Upper Band'], 
                    df_Boll['Lower Band'], 
                    color=band_color)

    ax.plot(x_axis, df_Boll['Close'], color=color1, lw=2)
    ax.plot(x_axis, df_Boll['30d mavg'], color=color2, lw=2)

    # Set Title & Show the Image
    ax.set_title('January 2020 to Current Day Bollinger Band For '+stock)
    ax.set_xlabel('Date (Year/Month)')
    ax.set_ylabel('Price(USD)')
    plt.show()
    return ax


# In[22]:


df = pdr.get_data_yahoo('AMZN', '2000-01-01')
create_plots(df,'AMZN','blue','orange','navy')


# In[23]:


df_tsla = pdr.get_data_yahoo('TSLA', '2000-01-01')
create_plots(df_tsla,'TSLA','black','red','grey')


# In[24]:


df_blizzard = pdr.get_data_yahoo('ATVI', '2000-01-01')
create_plots(df_blizzard,'ATVI','blue','gold','black')


# In[25]:


df_nintendo = pdr.get_data_yahoo('NTDOY', '2000-01-01')
nintendo_ax = create_plots(df_nintendo,'NTDOY','red','cyan','lightgrey')


# In[16]:


plt.style.use('bmh')
fig = plt.figure(figsize=(20, 10))
#axes1 = fig.add_axes([0.1, 0.1, 0.8, 0.8])
axes2 = fig.add_axes([0.18, 0.6, 0.3, 0.3])
axes3 = fig.add_axes([0.50, 0.95, 0.3, 0.3])
axes4 = fig.add_axes([0.18, 0.95, 0.3, 0.3])
axes5 = fig.add_axes([0.50, 0.6, 0.3, 0.3])
x = df_nintendo.index.date
y1 = df_nintendo['Close']
axes2.plot(x, y1, 'r')
axes2.set_title('Closing 2020 for Nintendo')

x = df_tsla.index.date
y1 = df_tsla['Close']
axes5.plot(x, y1, 'g')

x = df_blizzard.index.date
y1 = df_blizzard['Close']
axes3.plot(x, y1, 'b')

x = df.index.date
y1 = df['Close']
axes4.plot(x, y1, 'y')

axes2.set_title('Closing 2020 for Nintendo')
axes3.set_title('Closing 2020 for Tesla')
axes4.set_title('Closing 2020 for Blizzard')
axes5.set_title('Closing 2020 for Amazon')
#print(df_nintendo)
#x = df_nintendo.index.date
#y1 = df_nintendo[]

plt.show()


# In[ ]:




