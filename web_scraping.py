from data_grabber import *
from plot_image import *
from slack_utils import *

if __name__ == "__main__":
    data = send_df('amzn')
    
    plot = create_plots(data,'blue','orange','navy')

    post_message_to_slack(plot)
    web_url = "https://apilayer.com/"
    get_site_reponse(web_url)
    res = get_site_reponse(web_url)
    if res.status_code == 200:
        post_image(plot)
    else:
        print("Website Isn't Up")